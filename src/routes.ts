import { Router } from 'express';
import { ensureAuthenticateClient } from './middlewares/ensureAuthenticateClient';
import { ensureAuthenticateDeliveryman } from './middlewares/ensureAuthenticateDeliveryman';
import { AuthenticateClientController } from './modules/account/useCases/authenticateClient/authenticateClientController';
import { AuthenticateDeliverymanController } from './modules/account/useCases/authenticateDeliveryman/authenticateDeliverymanController';
import { CreateClientController } from './modules/clients/useCases/createClient/CreateClientController';
import { FindAllDeliveriesController } from './modules/clients/useCases/FindAllDeliveries/FindAllDeliveriesController';
import { CreateDeliveryController } from './modules/delivery/useCases/createDelivery/createDeliveryController';
import { FindAllAvailableController } from './modules/delivery/useCases/findAllWithoutEndDate/findAllAvailableController';
import { UpdateDeliverymanController } from './modules/delivery/useCases/updateDeliveryman/updateDeliverymanController';
import { UpdateEndDateController } from './modules/delivery/useCases/updateEndDate/updateEndDateController';
import { CreateDeliverymanController } from './modules/deliveryman/useCases/createDeliveryman/createDeliverymanContoller';
import { FindAllDeliveriesDeliverymanController } from './modules/deliveryman/useCases/findAllDeliveries/FindAllDeliveriesDeliverymanController';

const routes = Router();

const createClientController = new CreateClientController();
const createDeliverymanController = new CreateDeliverymanController();
const createDeliveryController = new CreateDeliveryController();
const findAllAvailableController = new FindAllAvailableController();
const updateDeliverymanController = new UpdateDeliverymanController();
const updateEndDateController = new UpdateEndDateController()
const findAllDeliveriesController = new FindAllDeliveriesController();
const findAllDeliveriesDeliverymanController = new FindAllDeliveriesDeliverymanController();
const authenticateClientController = new AuthenticateClientController();
const authenticateDeliverymanController = new AuthenticateDeliverymanController();

routes.post('/client', createClientController.handle);
routes.post('/client/authenticate', authenticateClientController.handle);
routes.get('/client/deliveries', 
    ensureAuthenticateClient,
    findAllDeliveriesController.handle
);

routes.post('/deliveryman', createDeliverymanController.handle);
routes.post('/deliveryman/authenticate', authenticateDeliverymanController.handle);
routes.get('/deliveryman/deliveries',
    ensureAuthenticateDeliveryman,
    findAllDeliveriesDeliverymanController.handle
);

routes.put('/delivery/accept/:id',
    ensureAuthenticateDeliveryman,
    updateDeliverymanController.handle
);
routes.put('/delivery/close/:id',
    ensureAuthenticateDeliveryman,
    updateEndDateController.handle
);
routes.get('/delivery/available', 
    ensureAuthenticateDeliveryman,
    findAllAvailableController.handle
);
routes.post('/delivery',
    ensureAuthenticateClient,
    createDeliveryController.handle
);

export { routes };