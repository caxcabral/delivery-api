import { hash } from "bcrypt";
import { prisma } from "../../../../database/prismaClient";

interface ICreateClient {
    username: string;
    password: string;
}

export class CreateClientUseCase {
    async execute({ password, username }: ICreateClient) {
        // Check if user exists
        const clientExists = await prisma.client.findFirst({
            where: {
                username: {
                    equals: username,
                    mode: 'insensitive'
                }
            }
        })

        if (clientExists) {
            throw new Error('Client already exists');
        }

        // Hashing the password
        
        const hashedPassword = await hash(password, 10);

        // Save client

        const client = await prisma.client.create({
            data: {
                username,
                password: hashedPassword
            },
        });

        return client;
    }
}