import { hash } from "bcrypt";
import { prisma } from "../../../../database/prismaClient";

interface ICreateDeliveryman{
    username: string,
    password: string
}

export class CreateDeliverymanUseCase {
    async execute( { username, password } : ICreateDeliveryman ) {
        // Receber username, password,
        
        // Verificar se username cadastrado
        const deliverymanExists = await prisma.deliveryman.findFirst({
            where: {
                username: {
                    equals: username,
                    mode: 'insensitive',
                }
            }
        })

        if (deliverymanExists) {
            throw new Error('Deliveryman already exists');
        }

        // Hashing the password
        const hashedPassword = await hash(password, 10);

        // Save deliveryman
        const deliveryman = await prisma.deliveryman.create({
            data: {
                username,
                password: hashedPassword
            },
        });

        return deliveryman;
    }
}