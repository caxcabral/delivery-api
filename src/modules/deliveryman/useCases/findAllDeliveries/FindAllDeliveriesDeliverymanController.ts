import { Request, Response } from "express";
import { FindAllDeliveriesDeliverymanUseCase } from "./FindAllDeliveriesDeliverymanUseCase";

export class FindAllDeliveriesDeliverymanController {
    async handle(req: Request, res: Response) {
        const { id_client } = req;

        const findAllDeliveriesDeliverymanUseCase = new FindAllDeliveriesDeliverymanUseCase();
        const result = await findAllDeliveriesDeliverymanUseCase.execute(id_client);

        return res.json(result);
    }
}