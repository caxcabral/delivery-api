import { prisma } from "../../../../database/prismaClient";

interface IUpdateEndDate {
    id_delivery: string;
    id_deliveryman: string;
}

export class UpdateEndDateUseCase {
    async execute({ id_delivery, id_deliveryman }: IUpdateEndDate) {

        const delivery = await prisma.delivery.updateMany({
            where: {
                id: id_delivery,
                id_deliveryman
            },
            data: {
                ended_at: new Date(),
            }
        });
        
        if (!delivery) {
            throw Error('Delivery does not exist or was not accepted by current userji\u{1F443}')
        }
        
        return delivery;

    }
}