import { compare } from "bcrypt";
import { prisma } from "../../../../database/prismaClient";
import { sign } from "jsonwebtoken";

interface IAuthenticateClient {
    username: string;
    password: string;
}

export class AuthenticateClientUseCase {
    async execute({username, password} : IAuthenticateClient) {
        // Receber username, password,
        
        // Verificar se username cadastrado
        const client = await prisma.client.findFirst({
            where: {
                username
            }
        })

        if (!client) {
            throw new Error('Username or password invalid!');
        }
        

        // Verificar se senha corresponde ao username
        const passwordMatch = await compare(password, client.password);
        
        if(!passwordMatch) {
            throw new Error('Username or password invalid!');
        }
        
        
        // Gerar o token
        const token = sign({ username },  'fc75801d131d80c89186ce5d064dc2ba', {
            subject: client.id,
            expiresIn: '1d'
        });

        return token;
    }
}