import { NextFunction, Request, Response } from "express";
import { verify } from "jsonwebtoken";


interface IPayload {
    sub: string;
}

export async function ensureAuthenticateClient(req: Request, res: Response, next: NextFunction) {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        return res.status(401).json({
            message: 'Token missing'
        });
    }

    // [0] = 'Bearer -'
    // [1] = token
    const [, token] = authHeader.split(' ');

    //verificar se o token é valido
    try {

        const { sub } =  verify(token, 'fc75801d131d80c89186ce5d064dc2ba') as IPayload;
        req.id_client = sub;

        return next();

    } catch(err) {

        return res.status(401).json({
            message: 'Invalid Token'
        });

    }
}